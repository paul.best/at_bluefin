# AT_BlueFin

*Resnet-50 [1] trained on the Acoustic Trends Blue Fin Library open access dataset [2]*

## Dependencies
`torch` `torchvision` `scipy` `soundfile` `numpy` `pandas` `tqdm` `argparse`

install with `pip install package-name`

## Project description

The CNN is ran using the run_model.py script.
For interface description run `python run_model.py --help`

The script forwards the CNN over a given list of sound files and writes a .csv file with chunk and class wise predictions.
The CNN was trained as a multi-label classifier over the dataset proposed by Miller et al. [2] (every station except kerguelen 2005).

Known classes are :
Bp_20Plus, Bp-Downsweep,  Bp_20Hz,  Bm_Ant-A, Bm_Ant-Z, Bm_Ant-B, and Bm_D.
Bp standing for fin whale (Balaenoptera Physalus) and Bm for blue whale (Balaenoptera Musculus)

The model reached an 0.66 mAP score over the test set (kerguelen 2005) and 0.89 over the train set (remaining data).

### Refererences
[1] He, Kaiming, et al. "Deep residual learning for image recognition." arXiv preprint arXiv:1512.03385 (2015).

[2] Miller, Brian S., et al. "An open access dataset for developing automated detectors of Antarctic baleen whale sounds and performance evaluation of two commonly used detectors." Scientific Reports 11.1 (2021): 1-18.


### Contact info
paul.best@univ-tln.fr
